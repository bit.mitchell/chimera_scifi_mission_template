class clones_equip
{
  name = "Clone Trooper Platoon Standard Loadout";

  resupply[] = {
    {ACE_fieldDressing, 12}, //FIELD DRESSING
    {ACE_elasticBandage, 12}, //ELASTIC BANDAGE
    {ACE_packingBandage, 12}, //PACKING BANDAGE
    {ACE_splint, 4}, //SPLINT
    {BIT_AidKit, 2}, //AID KIT
    {SWLW_DC15A_Mag, 30}, //RIFLEMAN AMMO
    {SWLW_DC17_Mag, 10}, //SIDEARM AMMO
    {SWLW_DC15SAW_Mag, 5}, //AR AMMO
    {SWLW_DC15X_Mag, 8}, //SNIPER AMMO
    {JLTS_RPS6_mag, 6}, //LAUNCHER AMMO
    {3Rnd_SmokeRed_Grenade_shell, 5}, //GL
    {3Rnd_SmokeGreen_Grenade_shell, 5}, //GL
    {3Rnd_Smoke_Grenade_shell, 5}, //GL
    {SWLW_DC15A_UGL_smoke_red_Mag, 5}, //GL
    {SWLW_DC15A_UGL_smoke_green_Mag, 5}, //GL
    {SWLW_DC15A_UGL_smoke_white_Mag, 5}, //GL
    {SWLW_DC15A_UGL_Mag, 5}, //GL
    {ls_mag_classC_thermalDet, 10}, //FRAG
    {OPTRE_M2_Smoke, 10} //SMOKE
  };

  medical[] = {
    {BIT_AidKit, 5},
    {ACE_salineIV, 10},
    {ACE_salineIV_500, 20},
    {ACE_bandage, 50},
    {ACE_quikclot, 50},
    {ACE_elasticBandage, 30},
    {ACE_packingBandage, 30},
    {ACE_morphine, 20},
    {ACE_epinephrine, 20},
    {ACE_tourniquet, 10},
    {ACE_splint, 10}
  };


  class rm // DC15A - Rifleman
  {
    primary[] = {
      {"SWLW_DC15A", "", "", "", ""}
    };

    secondary = "SWLW_DC17";

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"SWLW_DC17_Mag", 5},
      {"SWLW_DC15A_Mag", 15},
      {"ls_mag_classC_thermalDet", 2},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {};
  };

  class gr : rm // DC15A UGL - Grenadier
  {
    primary[] = {
          {"SWLW_DC15A_ugl", "", "", "", ""}
    };

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"SWLW_DC15A_Mag", 10},
      {"SWLW_DC15A_UGL_smoke_red_Mag", 2},
      {"SWLW_DC15A_UGL_smoke_green_Mag", 2},
      {"SWLW_DC15A_UGL_smoke_white_Mag", 2},
      {"SWLW_DC15A_UGL_Mag", 4},
      {"ls_mag_classC_thermalDet", 6},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class lat : rm // DC15A - Rifleman (LAT)
  {
    primary[] = {
      {"SWLW_DC15A", "", "", "", ""}
    };
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"SWLW_DC15A_Mag", 15},
      {"OPTRE_M2_Smoke", 1},
      {"ls_mag_classC_thermalDet", 5},
      {"JLTS_RPS6_mag", 2}
    };

    secondary = "JLTS_RPS6";
  };

  class ar // DC15SAW - Autorifleman
  {
    primary[] = {{"SWLW_DC15SAW", "", "", "", ""}};

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"SWLW_DC15SAW_Mag", 6}
    };

    pack[] = {};
  };

  class aar // DC15A - Autorifleman Assistant
  {
    primary[] = {{"SWLW_DC15A", "", "", "", ""}};

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"SWLW_DC15A_Mag", 15},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {
      {"SWLW_DC15SAW_Mag", 6},
    };
  };

  class cls // DC15A - Medic
  {
    medic = 1;
    primary[] = {{"SWLW_DC15A", "", "", "", ""}};

    items[] = {
      CLS,
      {"ACRE_PRC343", 1},
      {"SWLW_DC15A_Mag", 15}
    };
  };

  class eng : rm // DC15A - Combat Engineer
  {
    primary[] = {{"SWLW_DC15A", "", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"SWLW_DC15A_Mag", 10},
      {"OPTRE_M2_Smoke", 4},
      {"SWLW_clones_spec_demo_mag", 5 },
      {"ACE_Clacker", 1 },
      {"ACE_Fortify", 1 }
    };
  };

  class dm // Sniper
  {
    primary[] = {
      {"SWLW_DC15X", "", "", "", ""}
    };

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"SWLW_DC15X_Mag", 10},
      {"ls_mag_classC_thermalDet", 2},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {};
  };

  class vc // Vehicle Crew
  {
    primary[] = {{"JLTS_DC15S", "", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 1},
      {"JLTS_DC15S_mag", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class hp // Heli Pilot
  {
    primary[] = {{"JLTS_DC15S", "", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 2},
      {"JLTS_DC15S_Mag", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class hc // Heli Crew
  {
    primary[] = {{"JLTS_DC15S", "", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"JLTS_DC15S_Mag", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class co : rm // Platoon Lead
  {
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 1},
      {"SWLW_DC15A_Mag", 15},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class fac // FAC
  {
    primary[] = {{"SWLW_DC15A", "", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 2},
      {"SWLW_DC15A_Mag", 15},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class sl : co // Squad Lead
  {
  };

  class tl : co // Team Lead
  {
  };
};