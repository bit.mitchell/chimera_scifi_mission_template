class unsc_odst_equip
{
  name = "UNSC ODST Standard Loadout";

  resupply[] = {
    {ACE_fieldDressing, 12}, //FIELD DRESSING
    {ACE_elasticBandage, 12}, //ELASTIC BANDAGE
    {ACE_packingBandage, 12}, //PACKING BANDAGE
    {ACE_splint, 4}, //SPLINT
    {BIT_AidKit, 2}, //AID KIT
    {OPTRE_32Rnd_762x51_Mag_Tracer_Yellow, 30}, //RM AMMO
    {OPTRE_8Rnd_127x40_Mag, 10}, //M6B
    {OPTRE_42Rnd_95x40_Mag_Tracer_Yellow, 5}, //M58S
    {OPTRE_10Rnd_127x99, 8}, //SRM77S1
    {OPTRE_M41_Twin_AI, 6}, //M41 SSR
    {3Rnd_SmokeRed_Grenade_shell, 5}, //GL
    {3Rnd_SmokeGreen_Grenade_shell, 5}, //GL
    {3Rnd_Smoke_Grenade_shell, 5}, //GL
    {OPTRE_3Rnd_SmokeRed_Grenade_shell, 5}, //GL
    {OPTRE_3Rnd_SmokeGreen_Grenade_shell, 5}, //GL
    {OPTRE_3Rnd_Smoke_Grenade_shell, 5}, //GL
    {1Rnd_HE_Grenade_shell, 5}, //GL
    {OPTRE_M9_Frag, 10}, //FRAG
    {OPTRE_M2_Smoke, 10}, //SMOKE
    {OPTRE_FC_BubbleShield, 1} //BUBBLE SHIELD
  };

  medical[] = {
    {DMNS_Biofoam, 5},
    {BIT_AidKit, 5},
    {ACE_salineIV, 10},
    {ACE_salineIV_500, 20},
    {ACE_bandage, 50},
    {ACE_quikclot, 50},
    {ACE_elasticBandage, 30},
    {ACE_packingBandage, 30},
    {ACE_morphine, 20},
    {ACE_epinephrine, 20},
    {ACE_tourniquet, 10},
    {ACE_splint, 10}
  };


  class rm // BR55 - Rifleman
  {
    primary[] = {
      {"OPTRE_MA37B", "optre_ma37_smartlink_scope", "optre_ma37ksuppressor", "", ""}
    };

    secondary = "OPTRE_M6B";

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 15},
      {"OPTRE_M9_Frag", 2},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {};
  };

  class gr : rm // MA5BGL - Grenadier
  {
    primary[] = {
          {"OPTRE_MA37BGL", "optre_ma37_smartlink_scope", "optre_ma37ksuppressor", "", ""}
    };

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 10},
      {"OPTRE_3Rnd_SmokeRed_Grenade_shell", 2},
      {"OPTRE_3Rnd_SmokeGreen_Grenade_shell", 2},
      {"OPTRE_3Rnd_Smoke_Grenade_shell", 2},
      {"1Rnd_HE_Grenade_shell", 4},
      {"OPTRE_M9_Frag", 6},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class lat : rm // MAB5 - Rifleman (LAT)
  {
    primary[] = {
      {"OPTRE_MA37B", "optre_ma37_smartlink_scope", "optre_ma37ksuppressor", "", ""}
    };
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 15},
      {"OPTRE_M2_Smoke", 1},
      {"OPTRE_M9_Frag", 5},
      {"OPTRE_M41_Twin_AI", 2}
    };

    secondary = "OPTRE_M41_SSR";
  };

  class ar // M73 - Autorifleman
  {
    primary[] = {{"OPTRE_M58S", "optre_m12_optic", "optre_ma37ksuppressor", "", ""}};

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_42Rnd_95x40_Mag_Tracer_Yellow", 10}
    };

    pack[] = {};
  };

  class aar // MAB5 - Autorifleman Assistant
  {
    primary[] = {{"OPTRE_MA37B", "optre_ma37_smartlink_scope", "optre_ma37ksuppressor", "", ""}};

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 15},
      {"OPTRE_42Rnd_95x40_Mag_Tracer_Yellow", 10},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {};
  };

  class cls // BR55 - Medic
  {
    medic = 1;
    primary[] = {{"OPTRE_MA37K", "optre_br55hb_scope", "optre_ma37ksuppressor", "", ""}};

    items[] = {
      CLS,
      {"ACRE_PRC343", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 15}
    };
  };

  class eng : rm // BR55 - Combat Engineer
  {
    primary[] = {{"OPTRE_MA37K", "optre_br55hb_scope", "optre_ma37ksuppressor", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 10},
      {"OPTRE_M2_Smoke", 4},
      {"OPTRE_FC_BubbleShield", 3 }, 
      {"UNSCMine_Range_Mag", 2}, 
      {"DemoCharge_Remote_Mag", 2}, 
      {"ACE_Clacker", 1 }, 
      {"ACE_Fortify", 1 } 
    };
  };

  class dm // Sniper
  {
    primary[] = {
      {"OPTRE_SRM77_S1", "optre_srm_sight", "optre_m7_laser", "bipod_01_f_blk", ""}
    };

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_10Rnd_127x99", 10},
      {"OPTRE_M9_Frag", 2},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {};
  };

  class vc // Vehicle Crew
  {
    primary[] = {{"OPTRE_MA37K", "optre_br55hb_scope", "optre_ma37ksuppressor", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class hp // Heli Pilot
  {
    primary[] = {{"OPTRE_MA37K", "optre_br55hb_scope", "optre_ma37ksuppressor", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 2},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class hc // Heli Crew
  {
    primary[] = {{"OPTRE_MA37K", "optre_br55hb_scope", "optre_ma37ksuppressor", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class co : rm // Platoon Lead
  {
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 1},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 15},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class fac // FAC
  {
    primary[] = {{"OPTRE_MA37B", "optre_ma37_smartlink_scope", "optre_ma37ksuppressor", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 2},
      {"OPTRE_32Rnd_762x51_Mag_Tracer_Yellow", 15},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class sl : co // Squad Lead
  {
  };

  class tl : co // Team Lead
  {
  };
};